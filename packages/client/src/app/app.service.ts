import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';

import { MobileAppItem } from '../../../interfaces/src/index';

@Injectable({ providedIn: 'root' })
export class AppService {

	httpOptions = {
		headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
	};

	constructor(private http: HttpClient) {}

	/** GET apps from the server */
	getApps(): Observable<MobileAppItem[]> {
		return this.http
			.get<MobileAppItem[]>('http://192.168.1.111:3000/apps')
	}

	/** POST: seed apps from the server */
	seedApps() {
		return this.http
			.post('http://192.168.1.111:3000/apps/seed', null)
	}

	/** POST: clear apps from the server */
	clearApps() {
		return this.http
			.post('http://192.168.1.111:3000/apps/clear', null)
	}
}