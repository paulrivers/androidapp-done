import { Pipe, PipeTransform } from '@angular/core';
import { MobileAppItem } from '../../../interfaces/src';

@Pipe({
  name: 'filter'
})

//pipe to get all the first letters of our apps, without duplicates
export class FilterPipe implements PipeTransform {

  transform(value: MobileAppItem[]): Set<string> {
    if(!value) return new Set<string>()

    const alphabet = new Set<string>(['#'])
    for(const item of value){
      alphabet.add(item.name.charAt(0).toLocaleUpperCase())
    }
    return alphabet
  }

}
