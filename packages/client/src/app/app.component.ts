import { Component, HostListener, OnInit } from '@angular/core';
import { MobileAppItem } from '../../../interfaces/src';
import { AppService } from './app.service';
import { delay } from 'rxjs/operators';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
	apps: MobileAppItem[] = []; //apps list
	indexes: { [key: string]: number } = {}; //indexes of the first app containing each letter
	currentLetter: string = ''; //currentActiveLetter
	errorMessage: string = ''; //error message

	initialClickPosition?: number = 0; //used for pull request

	pulling: boolean = false; //indicates the status if we begin to pull
	loadingApps: boolean = false; //if we are loading the apps to display an indicator
	scrollingAlphabet: boolean = false; //if we are currently moving thourgh the sidebar, useful to blur other apps and the background

	pullHeight: number = 0; //the pulling distance

	@HostListener('window:scroll', ['$event']) // for window scroll events
	onScroll() {
		if (!this.scrollingAlphabet) {
			this.getTopElement();
		}
	}

	constructor(private appService: AppService) {}

	ngOnInit(): void {
		this.loadApps();
	}

	onAlphabetScroll(value: boolean) {
		this.scrollingAlphabet = value;
	}

	onLetterChange(value: string) {
		this.currentLetter = value;
		if (this.scrollingAlphabet === false) {
			this.scrollTo(value);
		}
	}

	onRetryHandler() {
		this.errorMessage = '';
		this.loadApps();
	}

	//we sort the apps alhpabetically and we store the indexes
	initApps(appsToSort: MobileAppItem[]) {
		this.apps = appsToSort.sort((a, b) =>
			a.name.toLocaleLowerCase() > b.name.toLocaleLowerCase() ? 1 : -1
		);
		this.setFirstIndexes(appsToSort);
		this.currentLetter = Object.keys(this.indexes)[0];
	}

	//load apps from the server via app.service
	loadApps() {
		this.loadingApps = true;
		this.appService.getApps().subscribe(
			(apps) => {
				this.initApps(apps);
				this.loadingApps = false;
			},
			() => {
				this.loadingApps = false;
				this.errorMessage =
					'If you want the apps, to press the button below you must';
			}
		);
	}

	//scrolls to a certain app of id=id
	scrollTo(id: string) {
		this.currentLetter = id;
		let el = document.getElementById(this.indexes[id].toString());
		const whereToY = el?.offsetTop;
		window.scroll({
			top: whereToY!,
			left: 0,
		});
	}

	setFirstIndexes(apps: MobileAppItem[]) {
		this.indexes = {};
		if (apps.length !== 0) {
			let letter = apps[0].name.charAt(0).toLocaleUpperCase();
			this.indexes[letter] = 0;
			for (let i = 1; i < apps.length; i++) {
				let currentLetter: string = apps[i].name.charAt(0).toLocaleUpperCase();
				if (letter != currentLetter) {
					this.indexes[currentLetter] = i;
				}
				letter = currentLetter;
			}
		}
	}

	//to know which element is at all times at the top of the page
	getTopElement() {
		let firstElement = document.getElementById('0')!;
		let appWidth = firstElement.getBoundingClientRect().right!;
		let appHeight =
			firstElement.getBoundingClientRect().height! +
			parseInt(window.getComputedStyle(firstElement).marginBottom!);
		let listAppWidth = document.getElementById('appList')?.getBoundingClientRect().width;
		let nAppsXRow = Math.trunc(listAppWidth! / appWidth);
		let row = Math.trunc(window.scrollY / appHeight);
		let id = row * nAppsXRow;
		this.currentLetter = this.apps[id].name.charAt(0).toLocaleUpperCase();
	}

	//touch pulling gesture starts
	onTouchStart(event?: any) {
		if (event.type === 'mousedown') this.initialClickPosition = event.clientY;
		else this.initialClickPosition = event?.touches[0].pageY;
		this.pulling = true;
	}

	onTouchMove(event?: any) {
		let currentPointerPosition =
			event.type === 'mousemove' ? event.clientY : event?.touches[0].pageY;
		if (this.pulling && window.scrollY === 0) {
			if (currentPointerPosition - this.initialClickPosition! > 50) {
				this.pullHeight = 50;
			} else {
				this.pullHeight = currentPointerPosition - this.initialClickPosition!;
			}
		}
	}

	//touch pulling gesture ends
	onTouchEnd() {
		this.pulling = false;
		if (this.pullHeight === 50) {
			this.appService.seedApps().subscribe(() => {
				this.pullHeight = 0;
				this.loadApps();
			});
		}
	}
}
