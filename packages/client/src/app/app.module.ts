import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import {
	BrowserModule,
	HAMMER_GESTURE_CONFIG,
	HammerGestureConfig,
	HammerModule,
} from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { FilterPipe } from './filter.pipe';
import * as Hammer from 'hammerjs';
import { SideAlphabetComponent } from './side-alphabet/side-alphabet.component';
import { AppInterceptor } from './app.interceptor';

export class MyHammerConfig extends HammerGestureConfig {
	overrides = <any>{
		pan: { direction: Hammer.DIRECTION_VERTICAL },
	};
}

@NgModule({
	declarations: [AppComponent, FilterPipe, SideAlphabetComponent],
	imports: [BrowserModule, HttpClientModule, HammerModule],
	providers: [
		{
			provide: HAMMER_GESTURE_CONFIG,
			useClass: MyHammerConfig,
		},
		{ provide: HTTP_INTERCEPTORS, useClass: AppInterceptor, multi: true }
	],
	bootstrap: [AppComponent],
})
export class AppModule {}
