import { Injectable } from '@angular/core';
import {
	HttpRequest,
	HttpHandler,
	HttpEvent,
	HttpInterceptor,
	HttpErrorResponse,
} from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError, switchMap, retryWhen, concatMap, delay } from 'rxjs/operators';
import { AppService } from './app.service';

@Injectable()
export class AppInterceptor implements HttpInterceptor {
	constructor(private appService: AppService) {}

	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<any> {
		return next.handle(request).pipe(
      retryWhen(error => 
        error.pipe(
          concatMap((error, count) => {
            //if we get an error of apps already seeding and its a get apps method, we retry it 3 times with a wait time between retrys of 2 seconds. If after this we still see the same error, we throw it
            if (count <= 3 && error.status === 409 && request.method === "GET") {
              return of(error);
            }
            return throwError(error);
          }),
          delay(2000)
        )
      ),
			catchError((error: HttpErrorResponse) => {
        if(error.error.error === 'There are no apps') { //seed apps if we get a no error apps
          return this.seedApps().pipe(
            switchMap(() => next.handle(request))
          )
        }
				return throwError(error);
			})
		);
	}

  seedApps(): Observable<any> {
    return this.appService.seedApps()
  }
  getApps(): Observable<any> {
    return this.appService.getApps()
  }
}
