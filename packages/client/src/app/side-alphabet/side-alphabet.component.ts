import {
	Component,
	Input,
	Output,
	EventEmitter,
} from '@angular/core';

@Component({
	selector: 'app-side-alphabet',
	templateUrl: './side-alphabet.component.html',
	styleUrls: ['./side-alphabet.component.scss'],
})
export class SideAlphabetComponent{
	@Input() letters: { [key: string]: number } = {};
	@Input() currentLetter: string = '';
	@Output() alphabetScrolling = new EventEmitter<boolean>();
	@Output() letterChanged = new EventEmitter<string>();

	scrollingAlphabet: boolean = false;

	constructor() {}


	updateTouch(event?: any) {
    this.scrollingAlphabet = true;
		this.alphabetScrolling.emit(true);

		let spacingTop = document.getElementById('alphabetList')?.getBoundingClientRect().top;
		let alphabetHeight = document.getElementById('alphabetList')?.clientHeight;
		let currentYPosition = event.center.y;
		
		let index = Math.ceil(((currentYPosition - (spacingTop!)) / alphabetHeight!) * Object.keys(this.letters).length)
		let currentPointerLetter = Object.keys(this.letters)[index-1];

    this.letterChanged.emit(currentPointerLetter)

		if (currentPointerLetter !== '#' && this.currentLetter !== currentPointerLetter) {
			this.currentLetter = currentPointerLetter;
			let el = document.getElementById(this.letters[this.currentLetter].toString());
			el?.scrollIntoView();
		} 
	}

	onPanEndHandler() {
    this.scrollingAlphabet = false;
		this.alphabetScrolling.emit(false);
	}

  onLetterClicked(id: string) {
    this.letterChanged.emit(id)
  }
}
